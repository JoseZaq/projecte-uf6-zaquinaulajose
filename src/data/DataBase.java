package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBase {
    // VARS
    private final static String URL = "jdbc:postgresql://rogue.db.elephantsql.com:5432/iwcssjdp";
    private final static String BD = "iwcssjdp";
    private final static String USER = "iwcssjdp";
    private final static String PASS = "9WYz7xC8QVmftMOQ6CPb8h3SwPKKB77E";
    private Connection connection;
    /// get instance
    private static DataBase dataBase;
    public static DataBase getInstance(){
        if(dataBase == null)
            dataBase = new DataBase();
        return dataBase;
    }
    // CONSTRUCTOR
    public DataBase(){
        connection = null;
    }
    // methods
    public Connection connect(){
        try {
            connection = DriverManager.getConnection(URL, USER, PASS);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return connection;
    }
    public void close(){
        try {
            connection.close();
        } catch (SQLException throwables) {
            System.out.println("Error tancant la  BD");
        }
        connection = null;
    }
    // getters and setters

    public Connection getConnection() {
        return connection;
    }
}
