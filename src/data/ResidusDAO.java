package data;

import models.EmpresaProductora;
import models.Residus;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ResidusDAO {
    public static boolean insert(Residus res){
        try {
            Connection connection = DataBase.getInstance().getConnection();
            String query = "INSERT INTO residu VALUES(?,?,?,?,?)";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setString(1,res.getNif_empresa());
            insertStatement.setInt(2, res.getCod_residu());
            insertStatement.setInt(3, res.getToxicitat());
            insertStatement.setInt(4, res.getQuantitat_residu());
            insertStatement.setString(5, res.getAa_residu());
            insertStatement.execute();
            return true;
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }

    public static boolean delete(String nif, int codRes){
        try{
            Connection connection = DataBase.getInstance().getConnection();
            String query = "DELETE FROM residu WHERE nif_empresa = ? and " +
                    "cod_residu = ?";
            PreparedStatement deletStatement = connection.prepareStatement(query);
            deletStatement.setString(1, nif);
            deletStatement.setInt(2, codRes);
            deletStatement.execute();
            return true;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }
    public static boolean update(Residus res){
        try{
            Connection connection = DataBase.getInstance().getConnection();
            String query = "UPDATE residu SET(toxicitat, quantitat_residu" +
                    ", aa_residu) = (?, ?, ?) WHERE nif_empresa = ? and " +
                    "cod_residu = ? ";
            PreparedStatement updateStatement = connection.prepareStatement(query);
            updateStatement.setInt(1, res.getToxicitat());
            updateStatement.setInt(2,res.getQuantitat_residu());
            updateStatement.setString(3, res.getAa_residu());
            updateStatement.setString(4, res.getNif_empresa());
            updateStatement.setInt(5, res.getCod_residu());
            updateStatement.execute();
            return true;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }
    public static List<Residus> list(){
        List<Residus> list = new ArrayList<>();
        try{
            Connection connection = DataBase.getInstance().getConnection();
            String query = "SELECT * FROM residu ORDER BY nif_empresa";
            Statement listStatement = connection.createStatement();
            ResultSet result = listStatement.executeQuery(query);
            while(result.next()){
                String nif = result.getString("nif_empresa");
                int codRes = result.getInt("cod_residu");
                int toxicitat = result.getInt("toxicitat");
                int quantitat = result.getInt("quantitat_residu");
                String aa = result.getString("aa_residu");
                list.add(new Residus(nif,codRes,toxicitat,quantitat,aa));
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return  list;
    }

    public static Residus search(String nif, int codRes) {
        try{
            Connection connection = DataBase.getInstance().getConnection();
            String query = "SELECT * FROM residu WHERE nif_empresa = ? and " +
                    "cod_residu = ?";
            PreparedStatement selectStatment = connection.prepareStatement(query);
            selectStatment.setString(1, nif);
            selectStatment.setInt(2,codRes);
            ResultSet result = selectStatment.executeQuery();
            while(result.next()){

                return new Residus(result.getString("nif_empresa")
                        , result.getInt("cod_residu")
                        , result.getInt("toxicitat")
                        , result.getInt("quantitat_residu")
                        , result.getString("aa_residu")
                );
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return null;
    }
}
