package data;

import models.Desti;
import models.Residus;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DestiDAO {
    public static boolean insert(Desti des){
        try {
            Connection connection = DataBase.getInstance().getConnection();
            String query = "INSERT INTO desti VALUES(?,?,?,?)";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setString(1,des.getCod());
            insertStatement.setString(2,des.getNom());
            insertStatement.setString(3,des.getCiutat());
            insertStatement.setString(4,des.getAa());
            insertStatement.execute();
            return true;
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }

    public static boolean delete(String cod){
        try{
            Connection connection = DataBase.getInstance().getConnection();
            String query = "DELETE FROM desti WHERE cod_desti = ?";
            PreparedStatement deletStatement = connection.prepareStatement(query);
            deletStatement.setString(1, cod);
            deletStatement.execute();
            return true;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }
    public static boolean update(Desti des){
        try{
            Connection connection = DataBase.getInstance().getConnection();
            String query = "UPDATE desti SET(nom_desti, ciutat_desti" +
                    ", aa_desti) = ( ?, ?, ?) WHERE cod_desti = ? ";
            PreparedStatement updateStatement = connection.prepareStatement(query);
            updateStatement.setString(1,des.getNom());
            updateStatement.setString(2,des.getCiutat());
            updateStatement.setString(3,des.getAa());
            updateStatement.setString(4,des.getCod());
            updateStatement.execute();
            return true;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }
    public static Desti search(String cod) {
        try{
            Connection connection = DataBase.getInstance().getConnection();
            String query = "SELECT * FROM desti WHERE cod_desti = ?";
            PreparedStatement selectStatment = connection.prepareStatement(query);
            selectStatment.setString(1, cod);
            ResultSet result = selectStatment.executeQuery();
            while(result.next()){
                return new Desti(result.getString("cod_desti")
                        , result.getString("nom_desti")
                        , result.getString("ciutat_desti")
                        , result.getString("aa_desti")
                );
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return null;
    }
    public static List<Desti> list(){
        List<Desti> list = new ArrayList<>();
        try{
            Connection connection = DataBase.getInstance().getConnection();
            String query = "SELECT * FROM desti ORDER BY cod_desti";
            Statement listStatement = connection.createStatement();
            ResultSet result = listStatement.executeQuery(query);
            while(result.next()){
                String cod = result.getString("cod_desti");
                String nom = result.getString("nom_desti");
                String ciutat = result.getString("ciutat_desti");
                String aa = result.getString("aa_desti");
                list.add(new Desti(cod,nom,ciutat,aa));
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return  list;
    }
}
