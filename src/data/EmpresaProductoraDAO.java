package data;

import models.EmpresaProductora;
import models.Residus;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmpresaProductoraDAO {
    public static boolean insert(EmpresaProductora emp){
        try {
            Connection connection = DataBase.getInstance().getConnection();
            String query = "INSERT INTO empresaproductora VALUES(?,?,?,?,?)";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setString(1,emp.getNifEmpresa());
            insertStatement.setString(2,emp.getNomEmpresa());
            insertStatement.setString(3,emp.getCiutatEmpresa());
            insertStatement.setString(4,emp.getActividat());
            insertStatement.setString(5,emp.getAaEmpresa());
            insertStatement.execute();
            return true;
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }

    public static boolean delete(String nif){
        try{
            Connection connection = DataBase.getInstance().getConnection();
            String query = "DELETE FROM empresaproductora WHERE nif_empresa = ?";
            PreparedStatement deletStatement = connection.prepareStatement(query);
            deletStatement.setString(1, nif);
            deletStatement.execute();
            return true;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }
    public static boolean update(EmpresaProductora emp){
        try{
            Connection connection = DataBase.getInstance().getConnection();
            String query = "UPDATE empresaproductora SET(nom_empresa, ciutat_empresa" +
                    ", activitat, aa_empresa) = (?, ?, ?, ?) WHERE nif_empresa = ? ";
            PreparedStatement updateStatement = connection.prepareStatement(query);
            updateStatement.setString(1,emp.getNomEmpresa());
            updateStatement.setString(2,emp.getCiutatEmpresa());
            updateStatement.setString(3,emp.getActividat());
            updateStatement.setString(4,emp.getAaEmpresa());
            updateStatement.setString(5,emp.getNifEmpresa());
            updateStatement.execute();
            return true;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }
    public static List<EmpresaProductora> list(){
        List<EmpresaProductora> list = new ArrayList<>();
        try{
            Connection connection = DataBase.getInstance().getConnection();
            String query = "SELECT * FROM empresaproductora ORDER BY nif_empresa";
            Statement listStatement = connection.createStatement();
            ResultSet result = listStatement.executeQuery(query);
            while(result.next()){
                String nif = result.getString("nif_empresa");
                String nom = result.getString("nom_empresa");
                String ciutat = result.getString("ciutat_empresa");
                String activitat = result.getString("activitat");
                String aa = result.getString("aa_empresa");
                list.add(new EmpresaProductora(nif,nom,ciutat,activitat,aa));
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return  list;
    }
    public static EmpresaProductora search(String nif) {
        try{
            Connection connection = DataBase.getInstance().getConnection();
            String query = "SELECT * FROM empresaproductora WHERE nif_empresa = ?";
            PreparedStatement selectStatment = connection.prepareStatement(query);
            selectStatment.setString(1, nif);
            ResultSet result = selectStatment.executeQuery();
            while(result.next()){
                return new EmpresaProductora(result.getString("nif_empresa")
                        , result.getString("nom_empresa")
                        , result.getString("ciutat_empresa")
                        , result.getString("activitat")
                        , result.getString("aa_empresa")
                );
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return null;
    }
}
