package data;

import models.EmpresaProductora;
import models.Residus;
import models.Trasllat;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class TrasllatDAO {
    public static boolean insert(Trasllat tr){
        try {
            Connection connection = DataBase.getInstance().getConnection();
            String query = "INSERT INTO trasllat VALUES(?,?,?,?,?,?,?,?,?)";
            PreparedStatement insertStatement = connection.prepareStatement(query);
            insertStatement.setString(1, tr.getNif_empresa());
            insertStatement.setInt(2,tr.getCod_residu());
            insertStatement.setDate(3,tr.getData_enviament());
            insertStatement.setString(4,tr.getCod_desti());
            insertStatement.setString(5,tr.getEnvas());
            insertStatement.setDate(6,tr.getData_arribada());
            insertStatement.setString(7,tr.getTractament());
            insertStatement.setInt(8,tr.getQuantitat_trasllat());
            insertStatement.setString(9,tr.getAa_trasllat());
            insertStatement.execute();
            return true;
        } catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }

    public static boolean delete(String nif, int codR, String data, String codD){
        try{
            Connection connection = DataBase.getInstance().getConnection();
            String query = "DELETE " +
                    "FROM trasllat " +
                    "WHERE nif_empresa = ? and cod_residu = ? and data_enviament = ? and cod_desti = ?";
            PreparedStatement deletStatement = connection.prepareStatement(query);
            deletStatement.setString(1, nif);
            deletStatement.setInt(2, codR);
            deletStatement.setDate(3, Date.valueOf(data));
            deletStatement.setString(4, codD);
            deletStatement.execute();
            return true;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }
    public static boolean update(Trasllat tr){
        try{
            Connection connection = DataBase.getInstance().getConnection();
            String query = "UPDATE trasllat SET(envas, data_arribada" +
                    ", tractament, quantitat_trasllat, aa_trasllat) = (?, ?, ?, ?, ?) " +
                    "WHERE nif_empresa = ? and cod_residu = ? and data_enviament = ? and cod_desti = ?";
            PreparedStatement updateStatement = connection.prepareStatement(query);
            updateStatement.setString(1, tr.getEnvas() );
            updateStatement.setDate(2, tr.getData_arribada());
            updateStatement.setString(3, tr.getTractament());
            updateStatement.setInt(4, tr.getQuantitat_trasllat());
            updateStatement.setString(5, tr.getAa_trasllat());
            updateStatement.setString(6, tr.getNif_empresa());
            updateStatement.setInt(7, tr.getCod_residu());
            updateStatement.setDate(8, tr.getData_enviament());
            updateStatement.setString(9, tr.getCod_desti());
            updateStatement.execute();
            return true;
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return false;
    }
    public static List<Trasllat> list(){
        List<Trasllat> list = new ArrayList<>();
        try{
            Connection connection = DataBase.getInstance().getConnection();
            String query = "SELECT * FROM trasllat ORDER BY nif_empresa";
            Statement listStatement = connection.createStatement();
            ResultSet result = listStatement.executeQuery(query);
            while(result.next()){
                String nif = result.getString("nif_empresa");
                int  codR = result.getInt("cod_residu");
                String  dataEn = result.getString("data_enviament");
                String  codD = result.getString("cod_desti");
                String  envas = result.getString("envas");
                String  dataArr = result.getString("data_arribada");
                String  tractament = result.getString("tractament");
                int  quant = result.getInt("quantitat_trasllat");
                String  aa = result.getString("aa_trasllat");
                list.add(new Trasllat(nif,codR,dataEn,codD,envas,dataArr,tractament,quant,aa));
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return  list;
    }
    public static Trasllat search(String nif, int codR, String data, String codD) {
        try{
            Connection connection = DataBase.getInstance().getConnection();
            String query = "SELECT * " +
                    "FROM trasllat " +
                    "WHERE nif_empresa = ? and cod_residu = ? and data_enviament = ? and cod_desti = ?";
            PreparedStatement selectStatment = connection.prepareStatement(query);
            selectStatment.setString(1, nif);
            selectStatment.setInt(2,codR);
            selectStatment.setDate(3,Date.valueOf(data));
            selectStatment.setString(4,codD);
            ResultSet result = selectStatment.executeQuery();
            while(result.next()){
                return new Trasllat(result.getString("nif_empresa")
                        , result.getInt("cod_residu")
                        , result.getString("data_enviament")
                        , result.getString("cod_desti")
                        , result.getString("envas")
                        , result.getString("data_arribada")
                        , result.getString("tractament")
                        , result.getInt("quantitat_trasllat")
                        , result.getString("aa_trasllat")
                );
            }
        }catch (SQLException e){
            System.out.println(e.getMessage());
        }
        return null;
    }
}
