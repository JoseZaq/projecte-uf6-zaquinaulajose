package models;

import ui.Printer;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Desti {
    //vars
    private String cod;
    private String nom;
    private String ciutat;
    private String aa;
    //contrcutors

    public Desti(String cod, String nom,
                 String ciutat, String aa) {
        this.cod = cod;
        this.nom = nom;
        this.ciutat = ciutat;
        this.aa = aa;
    }

    //methods
    public static Desti readDesti(Scanner scanner){
        System.out.print("Codi: ");
        String nif= scanner.next();
        System.out.print("Nom: ");
        scanner.nextLine();
        String nom = scanner.nextLine();
        System.out.print("Ciutat: ");
        String ciutat = scanner.nextLine();
        System.out.print("Descripció: ");
        String aa = scanner.nextLine();
        return new Desti(nif, nom,  ciutat, aa);
    }
    //getters and setters

    public String getCod() {
        return cod;
    }

    public String getNom() {
        return nom;
    }

    public String getCiutat() {
        return ciutat;
    }

    public String getAa() {
        return aa;
    }

    public String toStringPretty(List<Integer> columnsLenghts) {
        List<String> variables = Arrays.asList(cod, nom, ciutat, aa);
        return Printer.printColumn(variables,columnsLenghts);
    }
    @Override
    public String toString() {
        List<String> variables = Arrays.asList(cod, nom, ciutat, aa);
        return Printer.printColumn(variables);
    }
}
