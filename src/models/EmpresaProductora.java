package models;

import ui.Printer;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class EmpresaProductora {
    //vars
    private String nifEmpresa;
    private String nomEmpresa;
    private String ciutatEmpresa;
    private String actividat;
    private String aaEmpresa;
    // contructor
    public EmpresaProductora(String nifEmpresa, String nomEmpresa,
                             String ciutatEmpresa, String actividat, String aaEmpresa) {
        this.nifEmpresa = nifEmpresa;
        this.nomEmpresa = nomEmpresa;
        this.ciutatEmpresa = ciutatEmpresa;
        this.actividat = actividat;
        this.aaEmpresa = aaEmpresa;
    }
    // methods
    public static EmpresaProductora readEmpresaProductora(Scanner scanner){
        String nifEmpresa = scanner.next();
        scanner.nextLine();
        String nomEmpresa = scanner.nextLine();
        String ciutatEmpresa = scanner.nextLine();
        String actividat = scanner.nextLine();
        String aaEmpresa = scanner.nextLine();
        return new EmpresaProductora(nifEmpresa,nomEmpresa,ciutatEmpresa,actividat,aaEmpresa);
    }
    //getters and setters

    public String getNifEmpresa() {
        return nifEmpresa;
    }

    public String getNomEmpresa() {
        return nomEmpresa;
    }

    public String getCiutatEmpresa() {
        return ciutatEmpresa;
    }

    public String getActividat() {
        return actividat;
    }

    public String getAaEmpresa() {
        return aaEmpresa;
    }

    public String toStringPretty(List<Integer> maxColumnLenghts) {
        List<String> variables = Arrays.asList(nifEmpresa,nomEmpresa,ciutatEmpresa,actividat,aaEmpresa);
        return Printer.printColumn(variables,maxColumnLenghts);
    }
    @Override
    public String toString() {
        List<String> variables = Arrays.asList(nifEmpresa,nomEmpresa,ciutatEmpresa,actividat,aaEmpresa);
        return Printer.printColumn(variables);
    }
}
