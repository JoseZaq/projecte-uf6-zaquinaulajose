package models;

import ui.Printer;

import java.sql.Date;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Trasllat {
    // vars
    private String nif_empresa;
    private int cod_residu;
    private String data_enviament;
    private String cod_desti;
    private String envas;
    private String data_arribada;
    private String tractament;
    private int quantitat_trasllat;
    private String aa_trasllat;
    // contructor

    public Trasllat(String nif_empresa, int cod_residu, String data_enviament,
                    String cod_desti, String envas, String data_arribada,
                    String tractament, int quantitat_trasllat, String aa_trasllat) {
        this.nif_empresa = nif_empresa;
        this.cod_residu = cod_residu;
        this.data_enviament = data_enviament;
        this.cod_desti = cod_desti;
        this.envas = envas;
        this.data_arribada = data_arribada;
        this.tractament = tractament;
        this.quantitat_trasllat = quantitat_trasllat;
        this.aa_trasllat = aa_trasllat;
    }

    // methods
    public static Trasllat readTrasllat(Scanner scanner){
        System.out.print("NIF de la Empresa:");
        String nif_empresa = scanner.next();
        System.out.print("Codi de Residu:");
        int cod_residu = scanner.nextInt();
        System.out.print("Data de Enviament: ");
        String data_enviament = scanner.next();
        System.out.print("Codi de Desti: ");
        String cod_desti = scanner.next();
        scanner.nextLine();
        System.out.print("Tipo de Envas: ");
        String envas = scanner.nextLine();
        System.out.print("Data de Arribada: ");
        String data_arribada = scanner.nextLine();
        System.out.print("Tipo de Tractament: ");
        String tractament = scanner.nextLine();
        System.out.print("Quantitat:");
        int quantitat_trasllat = scanner.nextInt();
        scanner.nextLine();
        System.out.print("Descripció: ");
        String aa_trasllat = scanner.nextLine() ;
        return new Trasllat(nif_empresa,cod_residu,data_enviament,cod_desti,
                envas,data_arribada,tractament,quantitat_trasllat,aa_trasllat);
    }
    public String getNif_empresa() {
        return nif_empresa;
    }

    public int getCod_residu() {
        return cod_residu;
    }

    public Date getData_enviament() {
        return Date.valueOf(data_enviament);
    }

    public String getCod_desti() {
        return cod_desti;
    }

    public String getEnvas() {
        return envas;
    }

    public Date getData_arribada() {
        return Date.valueOf(data_arribada);
    }

    public String getTractament() {
        return tractament;
    }

    public int getQuantitat_trasllat() {
        return quantitat_trasllat;
    }

    public String getAa_trasllat() {
        return aa_trasllat;
    }

    public String toStringPretty(List<Integer> columnLenghts){
        List<String> variables = Arrays.asList(nif_empresa,Integer.toString(cod_residu),data_enviament,
                cod_desti,envas,data_arribada,tractament,Integer.toString(quantitat_trasllat),aa_trasllat);
        return Printer.printColumn(variables,columnLenghts);
    }
    @Override
    public String toString(){
        List<String> variables = Arrays.asList(nif_empresa,Integer.toString(cod_residu),data_enviament,
                cod_desti,envas,data_arribada,tractament,Integer.toString(quantitat_trasllat),aa_trasllat);
        return Printer.printColumn(variables);
    }
}
