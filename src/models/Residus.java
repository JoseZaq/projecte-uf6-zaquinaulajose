package models;

import ui.Printer;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Residus {
    //vars
    private String nif_empresa;
    private int cod_residu;
    private int toxicitat;
    private int quantitat_residu;
    private String aa_residu;
    //contructors

    public Residus(String nif_empresa, int cod_residu,
                   int toxicitat, int quantitat_residu, String aa_residu) {
        this.nif_empresa = nif_empresa;
        this.cod_residu = cod_residu;
        this.toxicitat = toxicitat;
        this.quantitat_residu = quantitat_residu;
        this.aa_residu = aa_residu;
    }

    //methods
    public static Residus readResidus(Scanner scanner){
        System.out.print("NIF: ");
        String nifEmpresa = scanner.next();
        System.out.print("Codi de residu: ");
        int cod = scanner.nextInt();
        System.out.print("Toxicitat: ");
        int tox = scanner.nextInt();
        System.out.print("Quantitat: ");
        int quan = scanner.nextInt();
        System.out.print("Descripció: ");
        scanner.nextLine();
        String aa = scanner.nextLine();
        return new Residus(nifEmpresa,cod,tox,quan,aa);
    }
    //getters and setters

    public String getNif_empresa() {
        return nif_empresa;
    }

    public int getCod_residu() {
        return cod_residu;
    }

    public int getToxicitat() {
        return toxicitat;
    }

    public int getQuantitat_residu() {
        return quantitat_residu;
    }

    public String getAa_residu() {
        return aa_residu;
    }

    public String toStringPretty(List<Integer> maxColumnLenghts) {
        List<String> variables = Arrays.asList(nif_empresa, Integer.toString(cod_residu), Integer.toString(toxicitat),
                Integer.toString(quantitat_residu), aa_residu);
        return Printer.printColumn(variables,maxColumnLenghts);
    }

    @Override
    public String toString() {
        List<String> variables = Arrays.asList(nif_empresa, Integer.toString(cod_residu), Integer.toString(toxicitat),
                Integer.toString(quantitat_residu), aa_residu);
        return Printer.printColumn(variables);
    }
}
