package ui;

import java.util.List;

/**
 * herramientas esteticas de impresion
 */
public class Printer {
    /**
     * imprime una cadena de caracteres entre dos barras
     * @param string cadena de caracteres a imprimir
     */
    public static void printTitle(String string){
        StringBuilder line= new StringBuilder();
        int lineSize = string.length()+8;
        line.append("-".repeat(Math.max(0, lineSize)));
        System.out.println(line+"\n"+"    "+string+"\n"+line);
    }
    public static void printRow(String string){
        StringBuilder line= new StringBuilder();
        int lineSize = string.length();
        line.append("\u001B[37m-\033[0m".repeat(lineSize));
        System.out.println(string+"\n"+line);
    }
    public static String printColumn(List<String> table,List<Integer> lenght){
        StringBuilder stringBuilder= new StringBuilder();
        for(int i=0 ; i < table.size(); i++){
            stringBuilder.append(String.format("%-"+lenght.get(i)+"s",table.get(i))).append(" \u001B[37m\u00A6\033[0m ");
        }
        return stringBuilder.toString();
    }
    public static String printColumn(List<String> table){
        StringBuilder stringBuilder= new StringBuilder();
        for (String s : table) {
            stringBuilder.append(String.format("%s", s)).append(" \u001B[37m\u00A6\033[0m ");
        }
        return stringBuilder.toString();
    }
}
