package ui.menus;

import java.util.Scanner;

public class mainMenu extends Menu{
    //vars
    DataBaseManagerMenu empresaProductora;
    DataBaseManagerMenu residus;
    DataBaseManagerMenu desti;
    DataBaseManagerMenu trasllat;
    //contructor
    public mainMenu(Scanner scanner) {
        super(scanner);
        empresaProductora = new EmpresaProductoraMenu(scanner);
        residus = new ResidusMenu(scanner);
        desti = new DestiMenu(scanner);
        trasllat = new TrasllatMenu(scanner);
        addTitle("Menu Principal");
        addOption("Empresa Productora");
        addOption("Residu");
        addOption("Desti");
        addOption("Trasllat");
    }
    // methods
    @Override
    public void action(int selected) {
        switch (selected){
            case 1:
                empresaProductora.showMenu();
                break;
            case 2:
                residus.showMenu();
                break;
            case 3:
                desti.showMenu();
                break;
            case 4:
                trasllat.showMenu();
                break;
        }
    }
}
