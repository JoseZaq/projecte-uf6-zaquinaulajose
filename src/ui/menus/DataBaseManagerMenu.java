package ui.menus;

import java.util.Scanner;

public abstract class DataBaseManagerMenu extends Menu{
    // contructor
    public DataBaseManagerMenu(Scanner scanner) {
        super(scanner);
        addOption("Llistar");
        addOption("Insert");
        addOption("Update");
        addOption("Delete");
        addOption("Select");
    }

    // methods
    @Override
    public void action(int selected) {
        switch (selected){
            case 1:
                list();
                break;
            case 2:
                insert();
                break;
            case 3:
                update();
                break;
            case 4:
                delete();
                break;
            case 5:
                search();
                break;
        }
    }

    protected abstract void list();
    protected abstract void insert();
    protected abstract void update();
    protected abstract void delete();
    protected  abstract void search();

}
