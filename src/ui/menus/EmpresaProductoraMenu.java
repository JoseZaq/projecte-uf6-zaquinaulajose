package ui.menus;

import data.EmpresaProductoraDAO;
import models.Desti;
import models.EmpresaProductora;
import ui.Printer;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class EmpresaProductoraMenu extends DataBaseManagerMenu {
    // contructor
    public EmpresaProductoraMenu(Scanner scanner) {
        super(scanner);
        addTitle("TABLA: Empresa Productora");
    }
    // methods
    @Override
    protected void list() {
        System.out.println("|     NIF     |    Nom    |    Ciutat    |          Activitat           |   AA   |");
        List<EmpresaProductora> empr = EmpresaProductoraDAO.list(); // no me deja hacer si nstatic
        List<Integer> maxCL = new ArrayList<>();
        //
        maxCL.add(empr.stream().map(EmpresaProductora::getNifEmpresa)
                .max(Comparator.comparingInt(x-> (x == null) ? 0:x.length())).get().length());
        maxCL.add(empr.stream().map(EmpresaProductora::getNomEmpresa)
                .max(Comparator.comparingInt(x-> (x == null) ? 0:x.length())).get().length());
        maxCL.add(empr.stream().map(EmpresaProductora::getCiutatEmpresa)
                .max(Comparator.comparingInt(x-> (x == null) ? 0:x.length())).get().length());
        maxCL.add(empr.stream().map(EmpresaProductora::getActividat)
                .max(Comparator.comparingInt(x-> (x == null) ? 0:x.length())).get().length());
        if(empr.stream().anyMatch(x -> x.getAaEmpresa() != null)){
            maxCL.add(empr.stream().map(EmpresaProductora::getAaEmpresa)
                    .max(Comparator.comparingInt(String::length)).get().length());
        }else{
            maxCL.add(5);
        }
        //
        empr.forEach(x -> Printer.printRow(x.toStringPretty(maxCL)));
    }
    @Override
    protected void insert() {
        System.out.println("Introdueix la nova Empresa Productora" +
                "(nif_empresa, nom_empresa, ciutat_empresa, activitat, aa_empresa): ");
        EmpresaProductora empresaProductora = EmpresaProductora.readEmpresaProductora(scanner);
        if(EmpresaProductoraDAO.insert(empresaProductora))
            System.out.println("Nou empresa ingresat correctament -> "+empresaProductora.toString());
    }
    @Override
    protected void update() {
        System.out.print("Introdueix la NIF de la Empresa Productora a modificar: ");
        String nifEmpresa = scanner.next();
        System.out.println("Introdueix les noves dades de la empresa" +
                "(nom_empresa, ciutat_empresa, activitat, aa_empresa):");
        scanner.nextLine();
        String nomEmpresa = scanner.nextLine();
        String ciutatEmpresa = scanner.nextLine();
        String actividat = scanner.nextLine();
        String aaEmpresa = scanner.nextLine();
        EmpresaProductora empresaProductora = new EmpresaProductora
                (nifEmpresa,nomEmpresa,ciutatEmpresa,actividat,aaEmpresa);
        if(EmpresaProductoraDAO.update(empresaProductora))
            System.out.println("Empresa actualizat correctament!-> "+empresaProductora.toString());
    }
    @Override
    protected void delete() {
        System.out.println("Introdueix el NIF de la empresa productora a eliminar:");
        String nif = scanner.next();
        EmpresaProductora emp = EmpresaProductoraDAO.search(nif);
        if(EmpresaProductoraDAO.delete(nif))
            System.out.println("Empresa esborrat! -> "+emp.toString());

    }
    @Override
    protected void search() {
        System.out.println("NIF:");
        String nif = scanner.next();
        EmpresaProductora empr = EmpresaProductoraDAO.search(nif);
        if(empr != null)
        System.out.println(empr.toString());
        else
            System.out.println("Los datos ingresados parecen estar incorrectos");
    }
}
