package ui.menus;

import data.ResidusDAO;
import data.TrasllatDAO;
import models.EmpresaProductora;
import models.Residus;
import ui.Printer;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class ResidusMenu extends DataBaseManagerMenu{
    // vars
    // contructors
    public ResidusMenu(Scanner scanner) {
        super(scanner);
        addTitle("TABLA: Residu ");
    }
    // methods
    @Override
    protected void list() {
        System.out.println("|    NIF    |Cod Res|Toxic| Quant | AA |");
        List<Residus> res = ResidusDAO.list();
        List<Integer> maxCL = new ArrayList<>();
        //
        maxCL.add(res.stream().map(Residus::getNif_empresa)
                .max(Comparator.comparingInt(String::length)).get().length());
        maxCL.add(res.stream().map(x->Integer.toString(x.getCod_residu()))
                .max(Comparator.comparingInt(String::length)).get().length());
        maxCL.add(res.stream().map(x->Integer.toString(x.getToxicitat()))
                .max(Comparator.comparingInt(String::length)).get().length());
        maxCL.add(res.stream().map(x->Integer.toString(x.getQuantitat_residu()))
                .max(Comparator.comparingInt(String::length)).get().length());
        if(res.stream().anyMatch(x -> x.getAa_residu() != null)) {
            maxCL.add(res.stream().map(Residus::getAa_residu)
                    .max(Comparator.comparingInt(String::length)).get().length());
        }else{
            maxCL.add(5);
        }
        //
        res.forEach(x -> Printer.printRow(x.toStringPretty(maxCL)));
    }

    @Override
    protected void insert() {
        System.out.println("Introdueix el nou residu" +
                "(nif_empresa, cod_residu , toxicitat, quantitat_residu , aa_residu): ");
        Residus residus = Residus.readResidus(scanner);
        if (ResidusDAO.insert(residus))
            System.out.println("Nou trasllat ingressat correctament!-> "+residus.toString());
    }

    @Override
    protected void update() {
        System.out.print("NIF : ");
        String nifEmpresa = scanner.next();
        System.out.print("Codi de residu : ");
        int codResidu = scanner.nextInt();
        System.out.println("Introdueix les noves dades del residu" +
                "(toxicitat, quantitat, aa):");
        System.out.print("Toxicitat: ");
        scanner.nextLine();
        int tox = scanner.nextInt();
        System.out.print("Quantitat: ");
        int quan = scanner.nextInt();
        System.out.print("Descripció: ");
        scanner.nextLine();
        String aa = scanner.nextLine();
        Residus residu = new Residus(nifEmpresa, codResidu, tox, quan, aa);
        if (ResidusDAO.update(residu))
            System.out.println("Trasllat actualizat correctament!-> "+residu.toString());
    }

    @Override
    protected void delete() {
        System.out.print("NIF:");
        String nif = scanner.next();
        System.out.print("codi de residu: ");
        int codi = scanner.nextInt();
        Residus res = ResidusDAO.search(nif,codi);
        if (ResidusDAO.delete(nif,codi))
            System.out.println("Trasllat esborrat correctament!-> "+res.toString());
    }

    @Override
    protected void search() {
        System.out.print("NIF:");
        String nif = scanner.next();
        System.out.print("codi de residu: ");
        int codi = scanner.nextInt();
        Residus residu = ResidusDAO.search(nif, codi);
        //
        if(residu != null)
        Printer.printRow(residu.toString());
        else
            System.out.println("Los datos ingresados parecen estar incorrectos");
    }
}
