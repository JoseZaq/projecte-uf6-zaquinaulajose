package ui.menus;

import data.DestiDAO;
import data.TrasllatDAO;
import models.Desti;
import models.Trasllat;
import ui.Printer;

import java.sql.Date;
import java.util.*;

public class TrasllatMenu extends DataBaseManagerMenu{
    //vars
    //contrcutors
    public TrasllatMenu(Scanner scanner) {
        super(scanner);
        addTitle("TABLA: Trasllat");
    }
    //methods
    @Override
    protected void list() {
        System.out.println("|   Nif de Empresa  | Codi de residu| Data de Enviament | Codi de Desti | " +
                "Envas | Data de Arribada | Tractament | Quantitat | Descripció");
        List<Trasllat> tr = TrasllatDAO.list();
        List<Integer> maxColumsLenght = new ArrayList<>();
        maxColumsLenght.add(tr.stream().map(Trasllat::getNif_empresa)
                .max(Comparator.comparingInt(String::length)).get().length());
        maxColumsLenght.add (tr.stream().map(x -> Integer.toString(x.getCod_residu()))
                .max(Comparator.comparingInt(String::length)).get().length());
        maxColumsLenght.add (tr.stream().map(x -> String.valueOf(x.getData_enviament()))
                .max(Comparator.comparingInt(String::length)).get().length());
        maxColumsLenght.add (tr.stream().map(Trasllat::getCod_desti)
                .max(Comparator.comparingInt(String::length)).get().length());
        maxColumsLenght.add (tr.stream().map(Trasllat::getEnvas)
                .max(Comparator.comparingInt(String::length)).get().length());
        maxColumsLenght.add( tr.stream().map(x->String.valueOf(x.getData_arribada()))
                .max(Comparator.comparingInt(String::length)).get().length());
        maxColumsLenght.add( tr.stream().map(Trasllat::getTractament)
                .max(Comparator.comparingInt(String::length)).get().length());
        maxColumsLenght.add( tr.stream().map(x -> Integer.toString(x.getQuantitat_trasllat()))
                .max(Comparator.comparingInt(String::length)).get().length());
        if(tr.stream().anyMatch(x -> x.getAa_trasllat() != null)) {
            maxColumsLenght.add(tr.stream().map(Trasllat::getAa_trasllat)
                    .max(Comparator.comparingInt(String::length)).get().length());
        }else{
            maxColumsLenght.add(5);
        }
        tr.forEach(x -> Printer.printRow(x.toStringPretty(maxColumsLenght)));
    }

    @Override
    protected void insert() {
        System.out.println("Introdueix el nou trasllat" +
                "(nif_empresa, cod_residu, data_enviament, cod_desti" +
                "envas, data_arribada, tractament, quantitat_trasllat, aa_trasllat): ");
        Trasllat tras = Trasllat.readTrasllat(scanner);
        if (TrasllatDAO.insert(tras))
            System.out.println("Nou trasllat ingressat correctament!-> "+tras.toString());
    }

    @Override
    protected void update() {
        System.out.print("Nif de Empresa: ");
        String nifEmpresa = scanner.next();
        System.out.print("Codi de Residu: ");
        int cod_residu = scanner.nextInt();
        System.out.print("Data de Enviament: ");
        String dataEnviament = scanner.next();
        System.out.print("Codi del Desti: ");
        String codDesti = scanner.next();
        scanner.nextLine();
        System.out.println("Introdueix les noves dades del trasllat" +
                "(envas,data_arribada, tractament, quantitat_trasllat, aa_trasllat):");
        System.out.print("Envas: ");
        String envas = scanner.nextLine();
        System.out.print("Data de Arribada: ");
        String dataArribada= scanner.nextLine();
        System.out.print("Tractament: ");
        String tract= scanner.nextLine();
        System.out.print("Quantitat: ");
        int quan= scanner.nextInt();
        scanner.nextLine();
        System.out.print("Descripció: ");
        String aa = scanner.nextLine();
        Trasllat tr = new Trasllat(nifEmpresa,cod_residu, dataEnviament, codDesti, envas, dataArribada,
                tract, quan, aa);
        if (TrasllatDAO.update(tr))
            System.out.println("Trasllat actualizat correctament!-> "+tr.toString());
    }

    @Override
    protected void delete() {

        System.out.print("Nif de Empresa: ");
        String nifEmpresa = scanner.next();
        System.out.print("Codi de Residu: ");
        int cod_residu = scanner.nextInt();
        System.out.print("Data de Enviament: ");
        String dataEnviament = scanner.next();
        System.out.print("Codi del Desti: ");
        String codDesti = scanner.next();
        Trasllat tr = TrasllatDAO.search(nifEmpresa, cod_residu, dataEnviament, codDesti);
        if(TrasllatDAO.delete(nifEmpresa, cod_residu, dataEnviament, codDesti))
            System.out.println("Traslllat esborrat -> "+tr.toString());
    }

    @Override
    protected void search() {
        System.out.print("Nif de Empresa: ");
        String nifEmpresa = scanner.next();
        System.out.print("Codi de Residu: ");
        int cod_residu = scanner.nextInt();
        System.out.print("Data de Enviament: ");
        String dataEnviament = scanner.next();
        System.out.print("Codi del Desti: ");
        String codDesti = scanner.next();
        Trasllat tr = TrasllatDAO.search(nifEmpresa, cod_residu, dataEnviament, codDesti);
        if(tr != null)
        System.out.println(tr.toString());
        else
            System.out.println("Los datos ingresados parecen estar incorrectos");

    }
}
