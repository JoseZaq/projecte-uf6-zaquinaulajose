package ui.menus;

import data.DestiDAO;
import models.Desti;
import ui.Printer;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class DestiMenu extends DataBaseManagerMenu{
    //vars
    //contructors
    public DestiMenu(Scanner scanner) {
        super(scanner);
        addTitle("TABLA: Desti");
    }
    //methods
    @Override
    protected void list() {
        System.out.println("|   Cod   |    Nom  | Ciutat  | AA |");
        List<Desti> des = DestiDAO.list();
        List<Integer> maxCL = new ArrayList<>();
        //
        maxCL.add(des.stream().map(Desti::getCod)
                .max(Comparator.comparingInt(String::length)).get().length());
        maxCL.add(des.stream().map(Desti::getNom)
                .max(Comparator.comparingInt(String::length)).get().length());
        maxCL.add(des.stream().map(Desti::getCiutat)
                .max(Comparator.comparingInt(String::length)).get().length());
        if(des.stream().anyMatch(x -> x.getAa() != null)) {
            maxCL.add(des.stream().map(Desti::getAa)
                    .max(Comparator.comparingInt(String::length)).get().length());
        }else{
            maxCL.add(5);
        }
        //
        des.forEach(x -> Printer.printRow(x.toStringPretty(maxCL)));
    }

    @Override
    protected void insert() {
        System.out.println("Introdueix el nou desti" +
                "(cod_desti, nom_desti, ciutat_desti, aa_desti): ");
        Desti emp = Desti.readDesti(scanner);
        if(DestiDAO.insert(emp))
            System.out.println("Nou desti ingresat correctament!-> "+emp.toString());
    }

    @Override
    protected void update() {
        System.out.print("Codi: ");
        String nifEmpresa = scanner.next();
        scanner.nextLine();
        System.out.println("Introdueix les noves dades del desti" +
                "(nom_desti, ciutat_desti, aa_desti):");
        System.out.print("Nom: ");
        String nom  = scanner.nextLine();
        System.out.print("Ciutat: ");
        String ciutat= scanner.nextLine();
        System.out.print("Descripció: ");
        String aa = scanner.nextLine();
        Desti emp = new Desti(nifEmpresa, nom, ciutat, aa);
        if(DestiDAO.update(emp))
            System.out.println("Desti actualizat correctament!-> "+emp.toString());
    }

    @Override
    protected void delete() {
        System.out.print("Codi:");
        String nif = scanner.next();
        Desti desti = DestiDAO.search(nif);
        if(DestiDAO.delete(nif))
            System.out.println("Desti esborrat!-> "+desti.toString());
    }

    @Override
    protected void search() {
        System.out.print("Codi:");
        String nif = scanner.next();
        Desti desti = DestiDAO.search(nif);
        if(desti != null)
        System.out.println(desti.toString());
        else
            System.out.println("Los datos ingresados parecen estar incorrectos");
    }
}
