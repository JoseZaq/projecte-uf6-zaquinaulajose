package ui.menus;

import ui.Printer;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public abstract class Menu {
    //vars
    List<String> options;
    Scanner scanner;
    String title;

    //constructor

    public Menu(Scanner scanner) {
        this.scanner = scanner;
        title = "";
        options = new ArrayList<>();
    }

    //methods
    public void addTitle(String title){
        this.title = title;
    }
    public void addOption(String option){
        options.add(option);
    }
    public int selectOption(){
        System.out.print("Opció: ");
        int n = scanner.nextInt();
        while( n < 0 && n > options.size()){
            System.out.println("Opción no válida");
            System.out.print("Opció: ");
            n = scanner.nextInt();
        }
        return n;
    }
    public void showMenu(){
        while(true){
            Printer.printTitle(title);
            for (int i = 0; i < options.size(); i++)
                System.out.printf("%d) %s\n", i + 1, options.get(i));
            System.out.println("0) Salir.");
            int selected = selectOption();
            if(selected == 0)
                break;
            else
                action(selected);
        }
    }
    public abstract void action(int selected);
}
