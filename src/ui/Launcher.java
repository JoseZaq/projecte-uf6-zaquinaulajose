package ui;

import data.DataBase;
import ui.menus.EmpresaProductoraMenu;
import ui.menus.Menu;
import ui.menus.mainMenu;

import java.util.Locale;
import java.util.Scanner;

public class Launcher {
    public static void main(String[] args) {
        Scanner scanner  = new Scanner(System.in).useLocale(Locale.US);
        DataBase.getInstance().connect();
        Menu mainMenu = new mainMenu(scanner);
        mainMenu.showMenu();
        DataBase.getInstance().close();
    }
}
